# README #

### What is this repository for? ###

* Application for complete disabling wifi/bluetooth radios on android devices when not connected for certain time and in scanning mode.
* 0.0.1

### How do I get set up? ###

* Our application will be availible on google play or you can download code and compile it yourself.
* Configuration is done in main program window, no need for anything else.
* Android 4.3 and higher is required to run this app.

### Who do I talk to? ###

* info@dezrodino.mzf.cz in case any question will have risen