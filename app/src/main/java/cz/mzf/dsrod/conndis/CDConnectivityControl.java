package cz.mzf.dsrod.conndis;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by DSroD on 28.6.2016.
 */
public class CDConnectivityControl extends BroadcastReceiver {

    protected Context ctx;
    protected ConnectivityManager conmgr;
    boolean cnctd;

    public CDConnectivityControl() {

        this.conmgr = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        this.ctx = MainActivity.ctx;
    }

    /**
     *
     * @return Returns boolean based on connection to ANY network;
     */
    protected boolean isConnected() {

        NetworkInfo nwInfo = conmgr.getActiveNetworkInfo();
        return nwInfo != null && nwInfo.isConnectedOrConnecting();

    }

    @Override
    public void onReceive(final Context ctx,final Intent itn)
    {

        this.cnctd = isConnected();

    }



}
