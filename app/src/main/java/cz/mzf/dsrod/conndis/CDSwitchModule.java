package cz.mzf.dsrod.conndis;

import android.content.Context;
import android.net.wifi.WifiManager;

import java.util.TimerTask;

/**
 * Created by DSroD on 30.6.2016.
 */
public class CDSwitchModule extends TimerTask {

    enum CDSwitchModuleType
    {
        WIFI, BT;
    }

    private CDSwitchModuleType type;
    private Context ctx;


    public CDSwitchModule(CDSwitchModuleType type)
    {

        this.type = type;
        this.ctx = MainActivity.ctx;

    }

    @Override
    public void run()
    {

        switch(this.type)
        {
            case WIFI:

                break;

            case BT:

                break;
        }

    }

}
