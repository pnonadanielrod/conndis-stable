package cz.mzf.dsrod.conndis;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;

import java.util.Timer;

/**
 * Created by DSroD on 28.6.2016.
 * This class uses CDConnectivityControl class to determine whether WiFi is connected to internet and provides methods to control WiFi device.
 */
public class CDWifiControl extends CDConnectivityControl {

    boolean isTurningOff = false;
    Timer tmr;
    private WifiManager wifiManager;

    public void SwitchWifiState() {

        wifiManager = (WifiManager) ctx.getSystemService(Context.WIFI_SERVICE);
        wifiManager.setWifiEnabled(!wifiManager.isWifiEnabled());

    }

    /**
     *
     * @return Returns boolean based on connection to WIFI network;
     */
    @Override
    protected boolean isConnected() {
        NetworkInfo nwInfo = conmgr.getActiveNetworkInfo();
        boolean cnctd = nwInfo != null && nwInfo.isConnectedOrConnecting();
        if(cnctd)
        {

            return nwInfo.getType() == ConnectivityManager.TYPE_WIFI;

        } else {

            return false;

        }

    }

    @Override
    public void onReceive(Context ctx, Intent itn) {
        super.onReceive(ctx, itn);
        if(cnctd == false && isTurningOff == false)
        {

            tmr = new Timer("wifiTimer");
            tmr.schedule(new CDSwitchModule(CDSwitchModule.CDSwitchModuleType.WIFI), MainActivity.wifiTimer);


        }

    }
}
