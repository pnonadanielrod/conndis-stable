package cz.mzf.dsrod.conndis;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    static long wifiTimer; /** Inactivity time allowed for WiFi */
    static long BTTimer;  /** Inactivity time allowed for bluetooth */
    static boolean running; /** TRUE if TURNED ON */
    static Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ctx = this;
    }
}
